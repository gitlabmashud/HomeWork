<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form</title>

    <style>
        label{
            width: 100px;
            display: inline-block;
            font-size:20px;
            font-weight:bold;
        }

        input[type='submit']{
            margin-left: 197px;
            width: 80px;
            background: #000;
            border: none;
            height: 30px;
            color: aliceblue;
            font-weight: bold;
            text-transform: uppercase;
            letter-spacing: 1px;
        }
    </style>
</head>
<body>
    <div style="width:500px;margin:auto;margin-top:50px;">
        <form action="result.php" method="post">
            <label>Name :</label>
            <input type="text" name="name">

            <br>
            <br>

            <label>Age :</label>
            <input type="number" name="age">

            <br>
            <br>

            <input type="submit" name="datapass" value="Submit">
        </form>

    </div>
</body>
</html>
