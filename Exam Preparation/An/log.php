<?php
$userChoice = $_POST['choice'];
$userName = $_POST['username'];
$userPassword = $_POST['password'];
$f1 = null;
$f2 = null;
$flag = null;
if(empty($userName) || empty($userPassword)) echo "<h2>Fill the fields first<br/><br/><a href='Login.html'>Go Back</a></h2>";
else{
    if($userChoice == 'Register')
    {
        //Check for user existance ----------------------
        $userList = file("login.txt");
        foreach($userList as $user){
            $checkName = explode('|',$user);
            if($checkName[0] == $userName){
                $flag = 1;
                break;
            }
        }
        //validate username..........................
        if(is_numeric(substr($userName , 0, 3)) ||is_numeric(substr($userName , 0, 2)) ||is_numeric(substr($userName , 0, 1)) || preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $userName)){
            $f1=1;
        }
        //validate password...........................
        if(strlen($userPassword)<6)
            $f2=1;
        if($flag==null){
            if($f1==1 && $f2==1){
                echo "<h2>ERROR-1 :  1st 3 character can't be numeric & username can't contain any special character</h2><br/><br/>
                    <h2>ERROR-2 :Password length must be greater then 5<br/><br/><a href='Login.html'>Go Back</a></h2>";
            }
            elseif ($f1==1){
                echo "<h2>ERROR-1 :  1st 3 character can't be numeric & username can't contain any special character<br/><br/><a href='Login.html'>Go Back</a></h2>";
            }
            elseif ($f2==1){
                echo "<h2>ERROR-2 :Password length must be greater then 5<br/><br/><a href='Login.html'>Go Back</a></h2>";
            }
            else{
                $input = $userName.'|'.$userPassword.'|'.'ppp';
                $loginFile = "login.txt";
                if(file_put_contents($loginFile, $input.PHP_EOL , FILE_APPEND | LOCK_EX))
                {
                    echo "<h2>Registration Successfull!!!</h2>";
                    echo "<br/><br/><h4><a href='Login.html'> Now Log In</a></h4>";
                }
                else "<h2>Registration Denied!!!</h2>";
            }
        }
        else{
            echo "<h2>Username exist!!!</h2>";
            echo "<br/><br/><h4><a href='Login.html'>Try another</a></h4>";
        }
    }
    else{
        $userList = file('login.txt');
        foreach($userList as $user)
        {
            $checkName = explode('|',$user);
            if(!empty($userName) && !empty($userPassword) && $checkName[0] == $userName && $checkName[1] == $userPassword) {
                $flag = 1;
                echo "<h3>Hello *".$userName."* welcome.</h3>";
                echo "<h3>You logged in to our system successfully...</h3>";
                echo "<br><br/><h2><a href='Login.html'>Log Out</a></h2>";
                break;
            }
        }
        if($flag==null) echo "<h2>Incorrect Username or Password<br><br><a href='Login.html'>Go Back</a></h2>";
    }
}
